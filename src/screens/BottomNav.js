import {View, Text} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Calendar from '../Component/Calendar';
import DropDown from '../Component/DropDown';

const BottomNav = () => {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator>
      <Tab.Screen name="Calendar" component={Calendar} />
      <Tab.Screen name="Dropdown" component={DropDown} />
    </Tab.Navigator>
  );
};

export default BottomNav;
