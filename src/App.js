import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  RefreshControl,
  FlatList,
  SectionList,
} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons';
import Calendar from './Component/Calendar';
import DropDown from './Component/DropDown';
import Just from './Component/Just';
import BottomNav from './screens/BottomNav';

const App = () => {
  const [Sections, setSections] = useState([
    {
      title: 'Title 1',
      data: ['Item 1-1', 'Item 1-2'],
    },
  ]);

  const Stack = createNativeStackNavigator();

  const onRefresh = () => {
    setRefreshing(true);
    const adding_index = Sections.length + 1;
    setSections([
      ...Sections,
      {
        title: 'Title ' + adding_index,
        data: ['Item ' + adding_index + '-1', 'Item ' + adding_index + '-2'],
      },
    ]);
    setRefreshing(false);
  };
  const [Refreshing, setRefreshing] = useState(false);

  return (
    // <SectionList
    //   keyExtractor={(item, index) => index.toString()}
    //   sections={Sections}
    //   renderItem={({ item }) => (
    //     <View style={styles.item}>
    //       <Text style={styles.text_item}>{item}</Text>
    //     </View >
    //   )}
    //   renderSectionHeader={({ section }) => (
    //     <View style={styles.header}>
    //       <Text style={styles.text_header}>{section.title}</Text>
    //     </View>
    //   )}
    //   refreshControl={
    //     < RefreshControl
    //       refreshing={Refreshing}
    //       onRefresh={onRefresh}
    //     />
    //   }
    // />
    // <>
    // <Calendar />
    // <DropDown/>
    // </>

    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="BottomNav" component={BottomNav} />
        <Stack.Screen name="Just" component={Just} />
      </Stack.Navigator>
      {/* <Tab.Navigator>
        <Tab.Screen name="Calendar" component={Calendar} />
        <Tab.Screen name="Dropdown" component={DropDown} />
        {/* <Stack.Screen name="Just" component={Just} /> 
      </Tab.Navigator> */}
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#4ae1fa',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
  },
  item: {
    borderBottomWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text_header: {
    color: '#000000',
    fontSize: 45,
    fontStyle: 'italic',
    margin: 10,
  },
  text_item: {
    color: '#000000',
    fontSize: 35,
    margin: 5,
  },
});

export default App;
