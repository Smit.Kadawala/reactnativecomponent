import React, {useRef, useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  FlatList,
  ScrollView,
  TouchableOpacity,
  Modal,
  Image,
} from 'react-native';
import {color} from 'react-native-reanimated';
import AsyncStorage from '@react-native-async-storage/async-storage';

const DropDown = () => {
  const [holiday, setHoliday] = useState();
  const [holidayYear, setHolidayYear] = useState('');
  const [years, setYears] = useState([
    2022, 2023, 2024, 2025, 2026, 2027, 2022, 2023, 2024, 2025, 2026, 2027,
    2022, 2023, 2024, 2025, 2026, 2027,
  ]);
  const [visible, setVisible] = useState(false);
  const itemHandle = item => {
    console.log('hello item', item.display_year);
    setVisible(!visible);
    // handleInput(item.label, questionId)
    setHolidayYear(item.display_year);
    holidaylistapi(item.display_year);
    // const value12 = item.label
    return item.display_year;
    // return item.label
  };
  useEffect(() => {
    holidayYearlistapi();
    holidaylistapi(new Date().getFullYear());
  }, []);

  async function holidaylistapi(holidayYear) {
    console.log('----->>>>>>>', holidayYear);
    let payload = {
      year: holidayYear,
    };
    let data = await PostApiRequest(API_HOLIDAY_LIST, payload, 0);
    if (data.error == false) {
      setHoliday(data.data);
    }
    if (data.error == true) {
      alert('Internal server error.');
    }
  }
  async function holidayYearlistapi() {
    const emp = await AsyncStorage.getItem('employeeId');
    let datetime_payload = {
      document: 'holiday',
      employee_id: emp,
    };
    let data = await PostApiRequest(
      API_VOC_DATE_TIME_LIST,
      datetime_payload,
      0,
    );
    if (data.error == false) {
      if (data.code == 'VOC_LIST_200') {
        // console.log("-----..",data.data)
        setYears(data.data);
      }
    }
    if (data.error == true) {
      alert('Internal server error.');
    }
  }
  return (
    <View style={styles.container}>
      {/* <Connectionpopup /> */}
      <Text style={styles.text}>Select Year:</Text>
      <View style={[styles.dropdown, {height: 50}]}>
        <TouchableOpacity
          onPress={() => {
            setVisible(!visible);
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={[styles.dropdownText]}>
                {holidayYear == '' ? 2022 : holidayYear}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
      {visible ? (
        <View style={[styles.dropdown1, {}]}>
          <FlatList
            data={years}
            style={[]}
            renderItem={({item}) => {
              console.log('=======>', item);
              return (
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#D3D3D3',
                    marginBottom: 3,
                    flex: 1,
                    margin: 5,
                  }}>
                  <TouchableOpacity
                    style={{width: '80%', padding: 7}}
                    onPress={() => {
                      setHolidayYear(item), setVisible(!visible);
                    }}>
                    <Text style={{color: 'green'}}>{item}</Text>
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
      ) : null}

      {/* <View style={{ marginTop: 10 }}>
                <FlatList
                    data={holiday}
                    style={{ height: '85%' }}
                    renderItem={({ item }) => (
                        <View >
                            <View style={styles.item}>
                                <View style={{ flex: 5, marginLeft: 20 }} >
                                    <Text style={styles.title}>{item.holiday_name}</Text>
                                    {item.Number_Of_Days > 1 ? (<Text style={{ marginTop: 5, color: '#818181' }}>{moment(item.start_date).format("DD/MM/YYYY")}{' TO '}{moment(item.end_date).format("DD/MM/YYYY")}</Text>) : (<Text style={{ marginTop: 5, color: '#818181' }}>{moment(item.start_date).format("DD/MM/YYYY")}</Text>)}

                                    <Text style={{ marginTop: 5, color: '#818181' }}>{item.WeekDays}</Text>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.count}>{item.Number_Of_Days}</Text>
                                    <Text style={[styles.text, { marginTop: -0 }]}>{item.Number_Of_Days > 1 ? 'Days' : 'Day'}</Text>
                                </View>
                            </View>
                        </View>
                    )}
                />
            </View> */}
    </View>
  );
};
export default DropDown;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: StatusBar.currentHeight || 0,
    backgroundColor: '#FFFFFF',
  },
  item: {
    backgroundColor: 'white',
    marginVertical: 8,
    flex: 1,
    flexDirection: 'row',
    elevation: 3,

    margin: 5,
    borderRadius: 10,
    marginHorizontal: 15,
    padding: 10,
  },

  id: {
    fontSize: 36,
    color: '#003c98',
    fontWeight: 'bold',
    marginTop: 12,
    marginLeft: 18,
    marginRight: 12,
  },
  title: {
    fontSize: 24,
    color: '#003c98',
    letterSpacing: 1,
  },
  count: {
    paddingTop: 10,
    fontSize: 30,
    color: '#003c98',
    fontWeight: 'bold',
  },
  dropdown: {
    borderWidth: 2,
    padding: 10,
    marginBottom: 10,
    margin: 5,
    borderRadius: 20,
    height: '35%',
    // backgroundColor:'red',
  },
  dropdown1: {
    borderWidth: 2,
    margin: 5,
    borderRadius: 20,
    height: '40%',
    width: '97.5%',
    marginTop: '24.3%',
    padingHorizontal: '2%',
    backgroundColor: '#ffffff',
    padding: '1%',
    position: 'absolute',
  },
  dropdownText: {
    fontSize: 18,
    paddingLeft: 22,
    color: '#003c98',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'red',
    borderRadius: 2,
    padding: 10,
    elevation: 5,
    width: '95%',
  },
  modalHeading: {
    flexDirection: 'row',
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#003c98',
    backgroundColor: '#f2f2f2',
    padding: 5,
    textAlign: 'center',
    alignItems: 'center',
  },
  modalHeadingText: {
    fontSize: 18,
    flex: 1,
    alignSelf: 'center',
    letterSpacing: 1,
    fontWeight: 'bold',
    color: '#818181',
    width: '100%',
    textAlign: 'center',
  },
  modalHeadingClose: {
    fontSize: 18,
    letterSpacing: 1,
    fontWeight: 'bold',
    color: '#818181',
  },
  listItems: {
    // height: 200,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    padding: 20,
    width: '100%',
  },
  listItemsActive: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: '#e2e2e2',
    backgroundColor: '#f2f2f2',
    padding: 10,
  },
  button: {
    borderRadius: 60,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  years: {
    backgroundColor: 'white',
    marginVertical: 8,
    width: 150,
    elevation: 5,
    margin: 5,
    borderRadius: 10,
    marginHorizontal: 15,
    padding: 10,
    alignItems: 'center',
    // elevation: 2,
  },

  text: {
    fontSize: 14,
    color: '#818181',
    paddingHorizontal: 15,
    marginTop: 20,
  },
  menuitem: {
    borderRadius: 10,
    position: 'absolute',
    width: '100%',
    top: 60,
    shadowColor: '#000000',
    shadowOffset: {height: 4, width: 0},
    shadowOpacity: 0.5,
  },
});
