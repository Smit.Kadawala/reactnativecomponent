import {View, Text, style} from 'react-native';
import React from 'react';
import CalendarPicker, {selectedDayColor} from 'react-native-calendar-picker';

const Calendar = ({navigation}) => {
  const minDate = new Date();

  const presentDate = date => {
    console.log('dattttteeeee', date);
    navigation.navigate('Just');
    // return (
    //   <View style={{backgroundColor: '#a45ed0'}}>
    //     <Text>Hello World</Text>
    //   </View>
    // );
  };
  return (
    <View>
      <CalendarPicker
        // minDate={minDate}
        todayBackgroundColor="#0394fc"
        scrollable="true"
        // selectedDayColor="#0394fc"
        showDayStragglers="true"
        // allowRangeSelection="true"
        previousTitle="prev."
        nextTitle="next"
        disabledDates="false"
        // onPress={Event}
        enableDateChange="true"
        onDateChange={date => presentDate(date)}
      />
    </View>
  );
};

export default Calendar;
