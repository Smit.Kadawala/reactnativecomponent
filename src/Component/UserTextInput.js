import { StyleSheet, Text, View, TextInput } from 'react-native'
import React from 'react'

const UserTextInput = (props) => {
  return (
    <View>
      <TextInput 
        {...props}
        editable
        style={styles.placeHolderText}
        />
    </View>
  )
}

export default UserTextInput

const styles = StyleSheet.create({
    placeHolderText: {
        fontSize: 20,
    },
})

